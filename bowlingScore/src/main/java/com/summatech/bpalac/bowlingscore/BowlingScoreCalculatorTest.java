package com.summatech.bpalac.bowlingscore;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

public class BowlingScoreCalculatorTest {

	@Test
	public void testPerfectGame() {
		ArrayList<Integer> rolls = new ArrayList<Integer>();
		for (int i = 0; i < 12; i++) {
			rolls.add(10);
		}
		int score = BowlingScoreCalculator.sumScore(rolls);
		Assert.assertEquals(300, score);
	}

	@Test
	public void testGutterGame() {
		ArrayList<Integer> rolls = new ArrayList<Integer>();
		for (int i = 0; i < 20; i++) {
			rolls.add(0);
		}
		int score = BowlingScoreCalculator.sumScore(rolls);
		Assert.assertEquals(0, score);
	}

	@Test
	public void testFourGame() {
		ArrayList<Integer> rolls = new ArrayList<Integer>();
		for (int i = 0; i < 20; i++) {
			rolls.add(4);
		}
		int score = BowlingScoreCalculator.sumScore(rolls);
		Assert.assertEquals(80, score);
	}

	@Test
	public void testFiveGame() {
		ArrayList<Integer> rolls = new ArrayList<Integer>();
		for (int i = 0; i < 21; i++) {
			rolls.add(5);
		}
		int score = BowlingScoreCalculator.sumScore(rolls);
		Assert.assertEquals(150, score);
	}

	@Test
	public void testRandomGame() {
		ArrayList<Integer> rolls = new ArrayList<Integer>();
		int[] rollArray = new int[]{0, 7, 10, 10, 3, 4, 9, 1, 4, 0, 10, 8, 2,
				1, 6, 10, 3, 5};
		for (int r : rollArray) {
			rolls.add(r);
			System.out.println(r + ": "
					+ BowlingScoreCalculator.sumScore(rolls));
		}
		int score = BowlingScoreCalculator.sumScore(rolls);
		Assert.assertEquals(128, score);
	}

}
