package com.summatech.bpalac.bowlingscore;

import java.util.ArrayList;

import com.summatech.bpalac.bowlingscore.ui.BowlingUI;

public class Score {

	public static void main(String[] args) {

		BowlingUI ui = new BowlingUI();

		int i = 0, j = 0; // Loop counters
		int frames = 1;
		int numOfPlayers;
		int score = 0;

		ui.text.pleaseEnterNumberOfPlayers();
		numOfPlayers = ui.input.getInt();

		String[] names = new String[numOfPlayers];
		int[] total = new int[numOfPlayers];

		ArrayList<ArrayList<Integer>> playerScore = createPlayerList(i,
				numOfPlayers);

		askForPlayerNames(i, numOfPlayers, names, ui);
		calculateScore(frames, numOfPlayers, total, names, score, ui,
				playerScore, j);
		ui.text.endOfGame(numOfPlayers, names, playerScore, total);

		ui.input.close();
	}

	public static ArrayList<ArrayList<Integer>> createPlayerList(int i, int num) {
		ArrayList<ArrayList<Integer>> playerScore = new ArrayList<ArrayList<Integer>>();

		for (i = 0; i < num; i++) {
			addPlayers(playerScore);
		}

		return playerScore;
	}

	public static void addPlayers(ArrayList<ArrayList<Integer>> playerScore) {
		ArrayList<Integer> players = new ArrayList<Integer>();
		playerScore.add(players);
	}

	public static void askForPlayerNames(int i, int num, String names[],
			BowlingUI ui) {
		if (num > 2) {
			greaterThanTwo(num, names, ui);
		}
		else if (num == 2) {
			equalToTwo(num, names, ui);
		}
		else {
			lessThanTwo(num, names, ui);
		}
	}

	public static void greaterThanTwo(int numOfPlayers, String names[],
			BowlingUI ui) {
		ui.text.pleaseEnterNames();
		inputNames(numOfPlayers, names, ui);
		ui.text.theNamesAre();
		ui.text.printMultipleNames(names);
	}

	public static void equalToTwo(int numOfPlayers, String names[], BowlingUI ui) {
		ui.text.pleaseEnterNames();
		inputNames(numOfPlayers, names, ui);
		ui.text.theNamesAre();
		ui.text.printTwoNames(names, numOfPlayers);
	}

	public static void lessThanTwo(int numOfPlayers, String names[],
			BowlingUI ui) {
		ui.text.pleaseEnterOneName();
		inputNames(numOfPlayers, names, ui);
		ui.text.theNameIs();
		ui.text.printOneName(names, numOfPlayers);
	}

	public static void inputNames(int numOfPlayers, String names[], BowlingUI ui) {
		for (int i = 0; i < numOfPlayers; i++) {
			names[i] = ui.input.getString();
		}
	}

	public static void calculateScore(int frames, int numOfPlayer, int total[],
			String names[], int score, BowlingUI ui,
			ArrayList<ArrayList<Integer>> playerScore, int j) {
		for (frames = 1; frames <= 10; frames++) {
			for (int i = 0; i < numOfPlayer; i++) {
				scoreDetails(names, frames, score, ui, playerScore, i, j, total);
			}
		}
	}

	public static void scoreDetails(String names[], int frames, int score,
			BowlingUI ui, ArrayList<ArrayList<Integer>> playerScore, int i,
			int j, int total[]) {
		score = getScore(names, frames, score, ui, playerScore, i);
		checkScore(names, frames, score, ui, playerScore, i, j, total);
		total[i] = BowlingScoreCalculator.sumScore(playerScore.get(i));
		totalSoFar(frames, playerScore, i, names, total, ui);
	}

	public static int getScore(String names[], int frames, int score,
			BowlingUI ui, ArrayList<ArrayList<Integer>> playerScore, int i) {
		ui.text.pleaseEnterScore(names, i, frames);
		score = inputScore(score, ui, playerScore, i);
		return score;
	}

	public static int inputScore(int score, BowlingUI ui,
			ArrayList<ArrayList<Integer>> playerScore, int i) {
		score = ui.input.getInt();
		playerScore.get(i).add(score);
		return score;
	}

	public static void checkScore(String names[], int frames, int score,
			BowlingUI ui, ArrayList<ArrayList<Integer>> playerScore, int i,
			int j, int total[]) {
		if (score != 10 && frames < 10) {
			checkOpenFrames(names, frames, score, ui, playerScore, i);
		}
		else if (score == 10 && frames < 10) {
			ui.text.printStrike();
		}
		else if (score != 10 && frames == 10) {
			checkFinal(names, frames, score, ui, playerScore, i);
		}
		else if (score == 10 && frames == 10) {
			checkFinalStrike(names, frames, score, ui, playerScore, i);
		}
	}

	public static void checkOpenFrames(String names[], int frames, int score,
			BowlingUI ui, ArrayList<ArrayList<Integer>> playerScore, int i) {
		score = secondPartFrame(names, frames, score, ui, playerScore, i);
		if ((playerScore.get(i).get(playerScore.get(i).size() - 2))
				+ (playerScore.get(i).get(playerScore.get(i).size() - 1)) == 10) {
			ui.text.printSpare();
		}
	}

	public static void checkFinal(String names[], int frames, int score,
			BowlingUI ui, ArrayList<ArrayList<Integer>> playerScore, int i) {
		score = secondPartFrame(names, frames, score, ui, playerScore, i);
		if ((playerScore.get(i).get(playerScore.get(i).size() - 2))
				+ (playerScore.get(i).get(playerScore.get(i).size() - 1)) == 10) {
			ui.text.printSpare();
			ui.text.printAnotherBowl();
			score = inputScore(score, ui, playerScore, i);
		}
	}

	public static int secondPartFrame(String names[], int frames, int score,
			BowlingUI ui, ArrayList<ArrayList<Integer>> playerScore, int i) {
		ui.text.pleaseEnterSecondScore(names, i, frames);
		score = inputScore(score, ui, playerScore, i);
		return score;
	}

	public static void checkFinalStrike(String names[], int frames, int score,
			BowlingUI ui, ArrayList<ArrayList<Integer>> playerScore, int i) {
		ui.text.printStrike();
		ui.text.printAnotherBowl();
		ui.text.pleaseEnterSecondScore(names, i, frames);
		score = inputScore(score, ui, playerScore, i);
		finalBowl(score, ui, playerScore, i);
	}

	public static void finalBowl(int score, BowlingUI ui,
			ArrayList<ArrayList<Integer>> playerScore, int i) {
		if (score != 10) {
			finalOpen(score, ui, playerScore, i);
		}
		else {
			finalStrike(score, ui, playerScore, i);
		}
	}

	public static void finalOpen(int score, BowlingUI ui,
			ArrayList<ArrayList<Integer>> playerScore, int i) {
		ui.text.printFinalBowl();
		score = inputScore(score, ui, playerScore, i);
	}

	public static void finalStrike(int score, BowlingUI ui,
			ArrayList<ArrayList<Integer>> playerScore, int i) {
		ui.text.printStrike();
		ui.text.printFinalBowl();
		score = inputScore(score, ui, playerScore, i);
	}

	public static void totalSoFar(int frames,
			ArrayList<ArrayList<Integer>> playerScore, int i, String names[],
			int total[], BowlingUI ui) {
		if (frames < 10) {
			totalOutput(playerScore, i, names, total, ui);
		}
	}

	public static void totalOutput(ArrayList<ArrayList<Integer>> playerScore,
			int i, String names[], int total[], BowlingUI ui) {
		if ((playerScore.get(i).get(playerScore.get(i).size() - 1) != 10)
				&& ((playerScore.get(i).get(playerScore.get(i).size() - 1)
						+ (playerScore.get(i)
								.get(playerScore.get(i).size() - 2)) != 10))) {
			ui.text.scoreSoFar(names, i, total);
		}
		else {
			ui.text.scoreNotCalculable(names, i);
		}
	}

}
