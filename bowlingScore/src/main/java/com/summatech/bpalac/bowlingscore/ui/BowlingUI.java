package com.summatech.bpalac.bowlingscore.ui;


/**
 * This class is responsible for all UI interactions.
 * 
 * @author bpalac
 * 
 */
public class BowlingUI {

	public BowlingScanner input = new BowlingScanner();
	public BowlingText text = new BowlingText();

}