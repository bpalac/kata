package com.summatech.bpalac.bowlingscore;

import java.util.ArrayList;

public class BowlingScoreCalculator {

	public static int sumScore(ArrayList<Integer> playerScore) {

		int totalScore = 0;
		int size = playerScore.size();

		// int current, next, nextNext;

		for (int j = 0; j < size; j++) {

			// current = 0;
			// next = 0;
			// nextNext = 0;

			// current = playerScore.get(j);

			/*
			 * if (j < size - 2) { next = playerScore.get(j + 1); } if (j < size
			 * - 3) { nextNext = playerScore.get(j + 2); }
			 */

			/*
			 * totalScore += current + next;
			 * 
			 * if (current + next >= 10) { totalScore += nextNext; } if (current
			 * < 10) { j++; }
			 */

			totalScore += playerScore.get(j);

			if (j < size - 3 && playerScore.get(j) == 10) {
				totalScore += playerScore.get(j + 1) + playerScore.get(j + 2);
			}
			else if (j < size - 3
					&& playerScore.get(j) + playerScore.get(j + 1) == 10) {
				totalScore += playerScore.get(j + 1) + playerScore.get(j + 2);
				j++;
			}

		}
		return totalScore;
	}
}
