package com.summatech.bpalac.bowlingscore.ui;

import java.util.Scanner;

/**
 * This class is responsible for gathering user input via the console.
 * 
 * @author bpalac
 * 
 */
public class BowlingScanner {

	private Scanner scanner = new Scanner(System.in);

	public String getString() {
		return scanner.next();
	}

	public int getInt() {
		return scanner.nextInt();
	}

	public void close() {
		scanner.close();
	}

}
