package com.summatech.bpalac.bowlingscore.ui;

import java.util.ArrayList;

/**
 * Handles all text output to the console.
 * 
 * @author bpalac
 * 
 */
public class BowlingText {

	public void pleaseEnterNumberOfPlayers() {
		print("Please enter the number of players: ");
	}

	public void pleaseEnterNames() {
		println("Please enter the names of the players (After each player's name, hit \"Enter\" or \"Space\".): ");
	}

	public void pleaseEnterOneName() {
		println("Please enter the name of the player: ");
	}

	public void theNamesAre() {
		print("The names of the players are: ");
	}

	public void theNameIs() {
		print("The name of the player is: ");
	}

	public void printMultipleNames(String names[]) {
		for (int i = 0; i < names.length - 1; i++) {
			print(names[i] + ", ");
		}
		print("and " + names[names.length - 1] + ".");
		println("");
	}

	public void printTwoNames(String names[], int numOfPlayers) {
		print(names[numOfPlayers - 2] + " and " + names[numOfPlayers - 1] + ".");
		println("");
	}

	public void printOneName(String names[], int numOfPlayers) {
		print(names[numOfPlayers - 1] + ".");
		println("");
	}

	public void pleaseEnterScore(String names[], int i, int frames) {
		println("Please enter " + names[i] + "'s frame " + (frames)
				+ " score: ");
	}

	public void pleaseEnterSecondScore(String names[], int i, int frames) {
		println("Please enter " + names[i] + "'s part 2 frame " + (frames)
				+ " score: ");
	}

	public void printStrike() {
		println("STRIKE!");
	}

	public void printSpare() {
		println("SPARE!");
	}

	public void scoreSoFar(String names[], int i, int total[]) {
		println(names[i] + "'s total score so far is: " + total[i]);
	}
	public void scoreNotCalculable(String names[], int i) {
		println(names[i] + "'s total score is indeterminable at the moment.");
	}

	public void printAnotherBowl() {
		println("You get another bowl!");
	}

	public void printFinalBowl() {
		println("Please enter your final bowl score.");
	}

	public void endOfGame(int numOfPlayers, String names[],
			ArrayList<ArrayList<Integer>> playerScore, int total[]) {
		println("Game over.");
		for (int i = 0; i < numOfPlayers; i++) {
			finalMessage(names, playerScore, total, i);
		}
	}

	private void finalMessage(String names[],
			ArrayList<ArrayList<Integer>> playerScore, int total[], int playerNumber) {
		print(names[playerNumber] + "'s set of frame scores are: " + playerScore.get(playerNumber));
		println("");
		println(names[playerNumber] + "'s total score is " + total[playerNumber]);
	}

	private void print(String text) {
		System.out.print(text);
	}

	private void println(String text) {
		System.out.println(text);
	}

}
