package com.summatech.bpalac.newbowling;

public class Application {

	public static void main(String[] args) {
		Game game = GameFactory.buildBowlingGame();
		game.start();
	}

}
