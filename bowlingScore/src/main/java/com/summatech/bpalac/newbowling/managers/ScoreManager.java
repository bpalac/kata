package com.summatech.bpalac.newbowling.managers;

import java.util.ArrayList;
import java.util.List;

import com.summatech.bpalac.newbowling.calculation.BowlingCalculator;
import com.summatech.bpalac.newbowling.messages.PrintOutput;

public class ScoreManager {

	private PrintOutput output;
	private BowlingCalculator calculator;
	private TiesManager tiesManager;

	private int[] totalScore;

	private List<List<Integer>> playerRolls;

	public ScoreManager(PrintOutput output, BowlingCalculator calculator,
			TiesManager tiesManager) {
		this.output = output;
		this.calculator = calculator;
		this.tiesManager = tiesManager;
	}

	public void initRollsList(int numberOfPlayers) {
		totalScore = new int[numberOfPlayers];

		playerRolls = new ArrayList<List<Integer>>();
		for (int currentPlayer = 0; currentPlayer < numberOfPlayers; currentPlayer++) {
			List<Integer> players = new ArrayList<Integer>();
			playerRolls.add(players);
		}
	}

	public int getScore(int playerNumber, int frame) {
		return playerRolls.get(playerNumber).get(frame);
	}

	public int getLatestScore(int playerNumber) {
		return getPreviousScore(playerNumber, 1);
	}

	public int getPreviousScore(int playerNumber, int numberOfFramesAgo) {
		return getScore(playerNumber, playerRolls.get(playerNumber).size()
				- numberOfFramesAgo);
	}

	public List<Integer> getScoreList(int playerNumber) {
		return playerRolls.get(playerNumber);
	}

	public int[] getTotalList() {
		return totalScore;
	}

	public void addScore(int currentPlayer, int rollScore) {
		playerRolls.get(currentPlayer).add(rollScore);
		updateTotal(currentPlayer);
	}

	private void updateTotal(int currentPlayer) {
		totalScore[currentPlayer] = calculator
				.score(getScoreList(currentPlayer));
	}

	public int getPlayerTotal(int currentPlayer) {
		return totalScore[currentPlayer];
	}

	public void checkForWinner(int numberOfPlayers, int[] totalScore,
			String[] names) {
		int highScore = 0;
		int winnerIndex = 0;
		String winner = "";

		for (int currentPlayer = 0; currentPlayer < numberOfPlayers; currentPlayer++) {
			if (totalScore[currentPlayer] > highScore) {
				highScore = totalScore[currentPlayer];
				winner = names[currentPlayer];
				winnerIndex = currentPlayer;
			}
		}
		if (!tiesManager.getTieScores().contains(highScore)) {
			tiesManager.revealTieScore();
			output.printWinner(winner, highScore);
		}
		else if (tiesManager.getTieScores().contains(highScore)) {
			output.printTieWin(highScore, winnerIndex,
					tiesManager.getHighestTieScore(winnerIndex),
					tiesManager.getHighestTieNames(winnerIndex));
			tiesManager.getRemainingTieScores(winnerIndex);
			tiesManager.getRemainingTieNames(winnerIndex);
			tiesManager.revealTieScore();
		}
	}
}
