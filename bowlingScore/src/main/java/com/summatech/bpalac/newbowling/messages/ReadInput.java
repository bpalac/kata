package com.summatech.bpalac.newbowling.messages;

import java.util.InputMismatchException;
import java.util.List;

import com.summatech.bpalac.newbowling.Exceptions;

public class ReadInput {

	private Exceptions exceptions;

	public ReadInput(Exceptions exceptions) {
		this.exceptions = exceptions;
	}

	public int getNumberOfPlayers() throws InputMismatchException {
		int numberOfPlayers = new InputScanner().getInt();
		exceptions.numberOfPlayersException(numberOfPlayers);
		return numberOfPlayers;
	}

	public String[] getNamesOfPlayers(int numberOfPlayers) {
		String[] names = new String[numberOfPlayers];
		for (int currentPlayer = 0; currentPlayer < numberOfPlayers; currentPlayer++) {
			names[currentPlayer] = getPlayerName();
		}
		return names;
	}

	private String getPlayerName() {
		return new InputScanner().getString();
	}

	public char getCorrectNames() throws InputMismatchException {
		char correctNames = new InputScanner().getChar();
		exceptions.yesNoException(correctNames);
		return correctNames;
	}

	public int getScore() throws InputMismatchException {
		int rollScore = new InputScanner().getInt();
		exceptions.scoreException(rollScore);
		return rollScore;
	}

	public int getSecondScore(List<Integer> rolls)
			throws InputMismatchException {
		int rollScore = new InputScanner().getInt();
		exceptions.secondScoreException(rolls, rollScore);
		return rollScore;
	}
}
