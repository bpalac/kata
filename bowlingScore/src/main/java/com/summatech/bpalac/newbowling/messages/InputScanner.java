package com.summatech.bpalac.newbowling.messages;

import java.util.Scanner;

public class InputScanner {

	private Scanner scanner = new Scanner(System.in);

	public String getString() {
		return scanner.next();
	}

	public char getChar() {
		return scanner.next().charAt(0);
	}

	public int getInt() {
		return scanner.nextInt();
	}

	public void closeScanner() {
		scanner.close();
	}

}
