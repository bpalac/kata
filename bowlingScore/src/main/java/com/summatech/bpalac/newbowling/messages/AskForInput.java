package com.summatech.bpalac.newbowling.messages;

public class AskForInput {

	public void enterNumberOfPlayers() {
		print(Messages.ENTER_NUMBER_OF_PLAYERS);
	}

	public void enterNamesOfMultiplePlayers(int numberOfPlayers) {
		println(Messages.ENTER_MULTIPLE_1 + numberOfPlayers
				+ Messages.ENTER_MULTIPLE_2);
	}

	public void enterNameOfOnePlayer(int numberOfPlayers) {
		print(Messages.ENTER_SINGLE_1 + numberOfPlayers
				+ Messages.ENTER_SINGLE_2);
	}

	public void askIfCorrect() {
		print(Messages.ASK_IF_CORRECT);
	}

	public void askForScore(String name, int frame) {
		println("");
		print(Messages.ASK_FOR_SCORE_1 + name + Messages.ASK_FOR_SCORE_2
				+ frame + Messages.ASK_FOR_FIRST_SCORE);
	}

	public void askForSecondScore(String name, int frame) {
		print(Messages.ASK_FOR_SCORE_1 + name + Messages.ASK_FOR_SCORE_2
				+ frame + Messages.ASK_FOR_SECOND_SCORE);
	}
	public void askForFinalScore(String name) {
		print(Messages.ASK_FOR_SCORE_1 + name + Messages.ASK_FOR_FINAL_SCORE);
	}

	private void print(String text) {
		System.out.print(text);
	}

	private void println(String text) {
		System.out.println(text);
	}

}
