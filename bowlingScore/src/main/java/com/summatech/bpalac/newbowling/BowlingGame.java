package com.summatech.bpalac.newbowling;

import com.summatech.bpalac.newbowling.managers.NamesManager;
import com.summatech.bpalac.newbowling.managers.ScoreManager;
import com.summatech.bpalac.newbowling.managers.TiesManager;
import com.summatech.bpalac.newbowling.messages.AskForInput;
import com.summatech.bpalac.newbowling.messages.PrintOutput;

public class BowlingGame implements Game {

	private AskForInput input;
	private NamesManager namesManager;
	private PrintOutput output;
	private ScoreManager scoreManager;
	private TiesManager tiesManager;
	private Validations validations;

	public BowlingGame(AskForInput input, NamesManager namesManager,
			PrintOutput output, ScoreManager scoreManager,
			TiesManager tiesManager, Validations validations) {
		this.input = input;
		this.namesManager = namesManager;
		this.output = output;
		this.scoreManager = scoreManager;
		this.tiesManager = tiesManager;
		this.validations = validations;
	}

	public void start() {
		start(false);
	}

	public void start(boolean isTest) {

		int numberOfPlayers = collectNumberOfPlayers();

		collectNames(numberOfPlayers);

		scoreManager.initRollsList(numberOfPlayers);

		// TODO remove this hack when done refactoring
		if (isTest) {
			return;
		}

		collectAllRolls(numberOfPlayers);

		output.gameOverMessage();
		checkForPerfectGames(numberOfPlayers);
		checkForTiesAndWin(numberOfPlayers);
	}

	private int collectNumberOfPlayers() {
		input.enterNumberOfPlayers();
		int numberOfPlayers = validations.numberOfPlayersValidation();
		return numberOfPlayers;
	}

	private void collectNames(int numberOfPlayers) {

		boolean areNamesCorrect = false;
		while (!areNamesCorrect) {

			if (numberOfPlayers > 1) {
				input.enterNamesOfMultiplePlayers(numberOfPlayers);
			}
			else {
				input.enterNameOfOnePlayer(numberOfPlayers);
			}

			namesManager.initNamesArray(numberOfPlayers);

			if (numberOfPlayers > 2) {
				output.printMoreThanTwoNames(numberOfPlayers);
			}
			else if (numberOfPlayers == 2) {
				output.printTwoNames(numberOfPlayers);
			}
			else {
				output.printOneName(numberOfPlayers);
			}

			input.askIfCorrect();
			areNamesCorrect = validations.getCorrectNamesValidation();
		}
	}

	private void collectAllRolls(int numberOfPlayers) {
		for (int frame = 1; frame <= 10; frame++) {
			for (int currentPlayer = 0; currentPlayer < numberOfPlayers; currentPlayer++) {
				int rollScore = collectFirstRoll(currentPlayer, frame);
				scoreManager.addScore(currentPlayer, rollScore);

				checkForRollValue(frame, currentPlayer, rollScore);

				revealScore(frame, currentPlayer);
			}
		}
	}

	private int collectFirstRoll(int currentPlayer, int frame) {
		input.askForScore(namesManager.getName(currentPlayer), frame);
		int rollScore = validations.scoreValidation();
		return rollScore;
	}

	private void checkForRollValue(int frame, int currentPlayer, int rollScore) {
		if (rollScore < 10) {
			lessThanTen(frame, currentPlayer);
		}
		else if (rollScore == 10) {
			equalToTen(frame, currentPlayer);
		}
	}

	private void lessThanTen(int frame, int currentPlayer) {
		int rollScore = collectSecondRoll(frame, currentPlayer);
		scoreManager.addScore(currentPlayer, rollScore);
		if (rollScore + scoreManager.getPreviousScore(currentPlayer, 2) == 10) {
			output.sparePrint(frame);
			if (frame == 10) {
				rollScore = collectFinalRoll(currentPlayer);
				scoreManager.addScore(currentPlayer, rollScore);
			}
		}
	}

	private void equalToTen(int frame, int currentPlayer) {
		checkForMultipleStrikes(frame, currentPlayer);
		if (frame == 10) {
			int rollScore = collectTenthFrameSecondRoll(currentPlayer, frame);
			scoreManager.addScore(currentPlayer, rollScore);
			checkForMultipleStrikes(frame, currentPlayer);

			rollScore = collectTenthFrameFinalRoll(currentPlayer);
			scoreManager.addScore(currentPlayer, rollScore);
			checkForMultipleStrikes(frame, currentPlayer);
		}
	}

	private int collectSecondRoll(int frame, int currentPlayer) {
		input.askForSecondScore(namesManager.getName(currentPlayer), frame);
		int rollScore = validations.secondScoreValidation(scoreManager
				.getScoreList(currentPlayer));
		return rollScore;
	}

	private int collectFinalRoll(int currentPlayer) {
		input.askForFinalScore(namesManager.getName(currentPlayer));
		int rollScore = validations.scoreValidation();
		return rollScore;
	}

	private int collectTenthFrameSecondRoll(int currentPlayer, int frame) {
		input.askForSecondScore(namesManager.getName(currentPlayer), frame);
		int rollScore = validations.scoreValidation();
		return rollScore;
	}

	private int collectTenthFrameFinalRoll(int currentPlayer) {
		input.askForFinalScore(namesManager.getName(currentPlayer));
		int rollScore = validations.scoreValidation();
		return rollScore;
	}

	private void checkForMultipleStrikes(int frame, int currentPlayer) {
		if (frame > 2 && scoreManager.getPreviousScore(currentPlayer, 3) == 10
				&& scoreManager.getPreviousScore(currentPlayer, 2) == 10) {
			output.turkeyPrint(frame);
		}
		else if (frame > 1
				&& scoreManager.getPreviousScore(currentPlayer, 2) == 10) {
			output.doublePrint(frame);
		}
		else {
			output.strikePrint(frame);
		}
	}

	private void revealScore(int frame, int currentPlayer) {
		int rollScore = scoreManager.getLatestScore(currentPlayer);
		if (frame < 10
				&& (rollScore == 10 || rollScore
						+ scoreManager.getPreviousScore(currentPlayer, 2) == 10)) {
			output.printNoScore(currentPlayer);
		}
		else if (frame < 10) {
			output.printScoreSoFar(currentPlayer,
					scoreManager.getPlayerTotal(currentPlayer));
		}
	}

	private void checkForPerfectGames(int numberOfPlayers) {
		for (int currentPlayer = 0; currentPlayer < numberOfPlayers; currentPlayer++) {
			int total = scoreManager.getPlayerTotal(currentPlayer);
			output.printFinalMessage(total, namesManager.getName(currentPlayer));
			if (total == 300) {
				output.printPerfectGame(namesManager.getName(currentPlayer));
			}
		}
	}

	private void checkForTiesAndWin(int numberOfPlayers) {
		if (numberOfPlayers > 1) {
			tiesManager.checkForTieScores(numberOfPlayers,
					scoreManager.getTotalList());

			scoreManager.checkForWinner(numberOfPlayers,
					scoreManager.getTotalList(), namesManager.getAllNames());
		}
	}
}
