package com.summatech.bpalac.newbowling.calculation;

import java.util.List;

public class BowlingCalculator {

	public int score(List<Integer> rolls) {
		int frame = 1;
		int total = 0;
		// Always handle a single frame each time through the loop.
		for (int roll = 0; roll < rolls.size() - 1 && frame <= 10; roll++) {
			total += rolls.get(roll) + rolls.get(roll + 1);
			if (roll < rolls.size() - 2
					&& (isStrike(rolls, roll) || isSpare(rolls, roll))) {
				total += rolls.get(roll + 2);
			}
			if (!isStrike(rolls, roll)) {
				roll++;
			}
			frame++;
		}
		return total;
	}

	private boolean isSpare(List<Integer> rolls, int roll) {
		return !rolls.get(roll).equals(10)
				&& rolls.get(roll) + rolls.get(roll + 1) == 10;
	}

	private boolean isStrike(List<Integer> rolls, int roll) {
		return rolls.get(roll).equals(10);
	}

}
