package com.summatech.bpalac.newbowling.messages;

public class Messages {

	public static final String ENTER_NUMBER_OF_PLAYERS = "Please enter the number of players: ";

	public static final String ENTER_MULTIPLE_1 = "Please enter the names of the ";
	public static final String ENTER_MULTIPLE_2 = " players. Press \"Enter\" after each name: ";

	public static final String ENTER_SINGLE_1 = "Please enter the name of the ";
	public static final String ENTER_SINGLE_2 = " player: ";

	public static final String ASK_IF_CORRECT = "Are these names correct? Please type \"Y\" for yes and \"N\" for no: ";

	public static final String ASK_FOR_SCORE_1 = "Please enter ";
	public static final String ASK_FOR_SCORE_2 = "'s frame ";

	public static final String ASK_FOR_FIRST_SCORE = " first roll score: ";

	public static final String ASK_FOR_SECOND_SCORE = " second roll score: ";

	public static final String ASK_FOR_FINAL_SCORE = "'s final roll score: ";

	public static final String MULTIPLE_NAMES = "The players are: ";
	public static final String SINGLE_NAME = "The player is: ";

	public static final String SCORE_SO_FAR = "'s score so far is: ";
	public static final String NO_SCORE = "'s score is indeterminable at the moment.";

	public static final String STRIKE = "STRIKE!";
	public static final String DOUBLE = "DOUBLE!";
	public static final String TURKEY = "TURKEY!";
	public static final String SPARE = "SPARE!";

	public static final String GAME_OVER = "GAME OVER.";

	public static final String FINAL_MESSAGE = "'s final score is: ";

	public static final String PERFECT_GAME = " HAD A PERFECT GAME!***";
	public static final String TIED_SCORE = " tied with a score of ";
	public static final String TIED_WIN = " tied with a winning score of ";
	public static final String WIN_SCORE = " wins the game with a score of ";

	public static final String INVALID_NUMBER_OF_PLAYERS = "ERROR! Please enter a number greater than 0: ";
	public static final String INVALID_CHARACTER = "Entry was not an acceptable character. Please enter \"Y\" for yes or \"N\" for no: ";
	public static final String INVALID_SCORE = "ERROR! Please enter a number greater than or equal to 0 and less than or equal to 10. Frame totals cannot be greater than 10: ";
}
