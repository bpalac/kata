package com.summatech.bpalac.newbowling.managers;

import java.util.ArrayList;
import java.util.List;

import com.summatech.bpalac.newbowling.messages.PrintOutput;

public class TiesManager {

	private PrintOutput output;
	private NamesManager namesManager;

	private List<Integer> tieScoreList;
	private List<List<String>> tieNames;

	public TiesManager(PrintOutput output, NamesManager namesManager) {
		this.output = output;
		this.namesManager = namesManager;
	}

	public void checkForTieScores(int numberOfPlayers, int[] totalScore) {
		int tieScore = 0;

		tieScoreList = initTieScoreList();
		tieNames = initTieNamesList(numberOfPlayers);

		for (int currentPlayer = 0; currentPlayer < numberOfPlayers; currentPlayer++) {
			tieScore = totalScore[currentPlayer];
			if (currentPlayer + 1 < totalScore.length
					&& tieScore == totalScore[currentPlayer + 1]) {
				tieScoreList = addTieScores(tieScore);
				tieNames = addFirstTieNames(currentPlayer);
			}
			for (int j = currentPlayer + 2; j < numberOfPlayers; j++) {
				if (tieScore == totalScore[j]) {
					tieNames = addMoreTieNames(
							namesManager.getName(currentPlayer), currentPlayer);
				}
			}
			if (currentPlayer + 1 < totalScore.length
					&& tieScore == totalScore[currentPlayer + 1]) {
				currentPlayer++;
			}
		}

		tieNames = removeNullLists();
	}

	private List<Integer> initTieScoreList() {
		List<Integer> tieScoreList = new ArrayList<Integer>();
		return tieScoreList;
	}

	private List<List<String>> initTieNamesList(int numberOfPlayers) {
		List<List<String>> tieNames = new ArrayList<List<String>>();
		for (int i = 0; i < numberOfPlayers; i++) {
			ArrayList<String> tiePlayers = new ArrayList<String>();
			tieNames.add(tiePlayers);
		}
		return tieNames;
	}

	private List<Integer> addTieScores(int tieScore) {
		tieScoreList.add(tieScore);
		return tieScoreList;
	}

	private List<List<String>> addFirstTieNames(int currentPlayer) {
		tieNames.get(currentPlayer).add(namesManager.getName(currentPlayer));
		tieNames.get(currentPlayer)
				.add(namesManager.getName(currentPlayer + 1));
		return tieNames;
	}

	private List<List<String>> addMoreTieNames(String name, int i) {
		tieNames.get(i).add(name);
		return tieNames;
	}

	private List<List<String>> removeNullLists() {
		for (int i = 0; i < tieNames.size(); i++) {
			if (tieNames.get(i).isEmpty()) {
				tieNames.remove(tieNames.get(i));
				i--;
			}
		}
		return tieNames;
	}

	public List<Integer> getTieScores() {
		return tieScoreList;
	}

	public List<List<String>> getTieNames() {
		return tieNames;
	}

	public int getHighestTieScore(int winnerIndex) {
		return tieScoreList.get(winnerIndex);
	}

	public List<String> getHighestTieNames(int winnerIndex) {
		return tieNames.get(winnerIndex);
	}

	public List<Integer> getRemainingTieScores(int winnerIndex) {
		tieScoreList.remove(winnerIndex);
		return tieScoreList;
	}

	public List<List<String>> getRemainingTieNames(int winnerIndex) {
		tieNames.remove(winnerIndex);
		return tieNames;
	}

	public void revealTieScore() {

		for (int i = 0; i < tieNames.size(); i++) {
			if (tieNames.get(i).size() > 2) {
				output.printMultipleTies(tieNames.get(i), tieScoreList, i);
			}
			else if (tieNames.get(i).size() == 2) {
				output.printOneTie(tieNames.get(i), tieScoreList, i);
			}
		}
	}
}
