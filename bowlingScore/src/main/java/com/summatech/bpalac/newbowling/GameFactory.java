package com.summatech.bpalac.newbowling;

import com.summatech.bpalac.newbowling.calculation.BowlingCalculator;
import com.summatech.bpalac.newbowling.managers.NamesManager;
import com.summatech.bpalac.newbowling.managers.ScoreManager;
import com.summatech.bpalac.newbowling.managers.TiesManager;
import com.summatech.bpalac.newbowling.messages.AskForInput;
import com.summatech.bpalac.newbowling.messages.PrintOutput;
import com.summatech.bpalac.newbowling.messages.ReadInput;

public class GameFactory {

	public static Game buildBowlingGame() {

		Exceptions exceptions = new Exceptions();
		BowlingCalculator calculator = new BowlingCalculator();

		ReadInput read = new ReadInput(exceptions);
		NamesManager namesManager = new NamesManager(read);
		PrintOutput output = new PrintOutput(namesManager);
		TiesManager tiesManager = new TiesManager(output, namesManager);
		ScoreManager scoreManager = new ScoreManager(output, calculator,
				tiesManager);

		return new BowlingGame(new AskForInput(), namesManager, output,
				scoreManager, tiesManager, new Validations(read, output));
	}

}
