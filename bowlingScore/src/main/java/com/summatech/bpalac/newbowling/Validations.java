package com.summatech.bpalac.newbowling;

import java.util.InputMismatchException;
import java.util.List;

import com.summatech.bpalac.newbowling.messages.PrintOutput;
import com.summatech.bpalac.newbowling.messages.ReadInput;

public class Validations {

	private ReadInput read;
	private PrintOutput output;

	public Validations(ReadInput read, PrintOutput output) {
		this.read = read;
		this.output = output;
	}

	public int numberOfPlayersValidation() {

		boolean isNumberOfPlayersValid = false;
		int numberOfPlayers = 0;

		while (!isNumberOfPlayersValid) {

			try {
				numberOfPlayers = read.getNumberOfPlayers();
				isNumberOfPlayersValid = true;
			}
			catch (InputMismatchException e) {
				// entry is not an integer
				output.invalidNumberOfPlayers();
			}
		}
		return numberOfPlayers;
	}

	public boolean getCorrectNamesValidation() {

		boolean correctNames = false;
		boolean continueLoop = true;
		char correctNamesChar;

		while (continueLoop) {

			try {
				correctNamesChar = read.getCorrectNames();
				continueLoop = false;
				if (Character.toLowerCase(correctNamesChar) == 'n') {
					correctNames = false;
				}
				else if (Character.toLowerCase(correctNamesChar) == 'y') {
					correctNames = true;
				}
			}
			catch (InputMismatchException e) {
				// entry is not y or n
				output.notAcceptableCharacterError();
			}
		}
		return correctNames;
	}

	public int scoreValidation() {

		boolean isScoreValid = false;
		int rollScore = 0;

		while (!isScoreValid) {

			try {
				rollScore = read.getScore();
				isScoreValid = true;
			}
			catch (InputMismatchException e) {
				// entry is not an integer
				output.invalidScore();
			}
		}
		return rollScore;
	}

	public int secondScoreValidation(List<Integer> rolls) {

		boolean isSecondScoreValid = false;
		int rollScore = 0;

		while (!isSecondScoreValid) {

			try {
				rollScore = read.getSecondScore(rolls);
				isSecondScoreValid = true;
			}
			catch (InputMismatchException e) { // entry is not an integer
				output.invalidScore();
			}
		}
		return rollScore;
	}
}
