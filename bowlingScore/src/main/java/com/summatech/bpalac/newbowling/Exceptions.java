package com.summatech.bpalac.newbowling;

import java.util.InputMismatchException;
import java.util.List;

public class Exceptions {

	public void numberOfPlayersException(int numberOfPlayers)
			throws InputMismatchException {
		if (numberOfPlayers <= 0) {
			InputMismatchException ex = new InputMismatchException(
					"Not a valid entry.");
			throw ex;
		}
	}

	public void yesNoException(char input) throws InputMismatchException {
		if (Character.toLowerCase(input) != 'n'
				&& Character.toLowerCase(input) != 'y') {
			InputMismatchException ex = new InputMismatchException(
					"Not a valid entry.");
			throw ex;
		}
	}

	public void scoreException(int rollScore) throws InputMismatchException {
		if (rollScore < 0 || rollScore > 10) {
			InputMismatchException ex = new InputMismatchException(
					"Not a valid entry.");
			throw ex;
		}
	}

	public void secondScoreException(List<Integer> rolls, int rollScore)
			throws InputMismatchException {
		if (rollScore < 0 || rollScore > 10
				|| rolls.get(rolls.size() - 1) + rollScore < 0
				|| rolls.get(rolls.size() - 1) + rollScore > 10) {
			InputMismatchException ex = new InputMismatchException(
					"Not a valid entry.");
			throw ex;
		}
	}
}
