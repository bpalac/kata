package com.summatech.bpalac.newbowling.messages;

import java.util.List;

import com.summatech.bpalac.newbowling.managers.NamesManager;

public class PrintOutput {

	private NamesManager namesManager;

	public PrintOutput(NamesManager namesManager) {
		this.namesManager = namesManager;
	}

	public String printMoreThanTwoNames(int numberOfPlayers) {

		StringBuilder builder = new StringBuilder();

		String nameMessageIntro = Messages.MULTIPLE_NAMES;
		for (int currentPlayer = 0; currentPlayer < numberOfPlayers - 1; currentPlayer++) {
			builder.append(namesManager.getName(currentPlayer) + ", ");
		}
		String nameMessageFinal = ("and "
				+ namesManager.getName(numberOfPlayers - 1) + ".");
		String nameMessage = nameMessageIntro + builder.toString()
				+ nameMessageFinal;
		println(nameMessage);
		return nameMessage;
	}

	public String printTwoNames(int numberOfPlayers) {
		String nameMessage = Messages.MULTIPLE_NAMES
				+ namesManager.getName(numberOfPlayers - 2) + " and "
				+ namesManager.getName(numberOfPlayers - 1) + ".";
		println(nameMessage);
		return nameMessage;
	}

	public String printOneName(int numberOfPlayers) {
		String nameMessage = Messages.SINGLE_NAME
				+ namesManager.getName(numberOfPlayers - 1) + ".";
		println(nameMessage);
		return nameMessage;
	}

	public void printScoreSoFar(int currentPlayer, int total) {
		println("");
		println(namesManager.getName(currentPlayer) + Messages.SCORE_SO_FAR
				+ total);
	}

	public void printNoScore(int currentPlayer) {
		println(namesManager.getName(currentPlayer) + Messages.NO_SCORE);
	}

	public void strikePrint(int frame) {
		println("");
		println(Messages.STRIKE);
		if (frame == 10) {
			println("");
		}
	}

	public void doublePrint(int frame) {
		println("");
		println(Messages.DOUBLE);
		if (frame == 10) {
			println("");
		}
	}

	public void turkeyPrint(int frame) {
		println("");
		println(Messages.TURKEY);
		if (frame == 10) {
			println("");
		}
	}

	public void sparePrint(int frame) {
		println("");
		println(Messages.SPARE);
		if (frame == 10) {
			println("");
		}
	}

	public void gameOverMessage() {
		println("");
		println(Messages.GAME_OVER);
		println("");
	}

	public void printFinalMessage(int total, String name) {
		println(name + Messages.FINAL_MESSAGE + total);
	}

	public void printPerfectGame(String name) {
		println("***" + name.toUpperCase() + Messages.PERFECT_GAME);
	}

	public void printMultipleTies(List<String> tieNames,
			List<Integer> tieScoreList, int currentPlayer) {
		println("");
		for (int j = 0; j < tieNames.size() - 1; j++) {
			print(tieNames.get(j) + ", ");
		}
		print("and " + tieNames.get(tieNames.size() - 1) + Messages.TIED_SCORE
				+ tieScoreList.get(currentPlayer) + ".");
	}

	public void printOneTie(List<String> tieNames, List<Integer> tieScoreList,
			int currentPlayer) {
		println("");
		print(tieNames.get(tieNames.size() - 2) + " and "
				+ tieNames.get(tieNames.size() - 1) + Messages.TIED_SCORE
				+ tieScoreList.get(currentPlayer) + ".");
	}

	public void printTieWin(int highScore, int winnerNumber,
			int winningTieScore, List<String> winningTieNames) {
		println("");
		if (winningTieNames.size() > 2) {
			for (int j = 0; j < winningTieNames.size() - 1; j++) {
				print(winningTieNames.get(j) + ", ");
			}
			print("and " + winningTieNames.get(winningTieNames.size() - 1)
					+ Messages.TIED_WIN + highScore + "!");
		}
		else if (winningTieNames.size() == 2) {
			print(winningTieNames.get(winningTieNames.size() - 2) + " and "
					+ winningTieNames.get(winningTieNames.size() - 1)
					+ Messages.TIED_WIN + highScore + "!");
		}
	}

	public void printWinner(String winner, int highScore) {
		println("\n");
		println(winner + Messages.WIN_SCORE + highScore + "!");
	}

	public void invalidNumberOfPlayers() {
		print(Messages.INVALID_NUMBER_OF_PLAYERS);
	}

	public void notAcceptableCharacterError() {
		print(Messages.INVALID_CHARACTER);
	}

	public void invalidScore() {
		print(Messages.INVALID_SCORE);
	}

	private void println(String text) {
		System.out.println(text);
	}

	private void print(String text) {
		System.out.print(text);
	}

}
