package com.summatech.bpalac.newbowling.managers;

import com.summatech.bpalac.newbowling.messages.ReadInput;

public class NamesManager {

	private ReadInput read;

	private String[] names;

	public NamesManager(ReadInput read) {
		this.read = read;
	}

	public void initNamesArray(int numberOfPlayers) {
		names = new String[numberOfPlayers];

		names = read.getNamesOfPlayers(numberOfPlayers);
	}

	public String getName(int playerNumber) {
		return names[playerNumber];
	}

	public String[] getAllNames() {
		return names;
	}
}
