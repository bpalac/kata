package com.summatech.bpalac.newbowling.calculation;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.summatech.bpalac.newbowling.messages.AskForInput;
import com.summatech.bpalac.newbowling.messages.PrintOutput;

public class BowlingCalculatorTest {

	private BowlingCalculator calculator = new BowlingCalculator();
	private AskForInput input = Mockito.mock(AskForInput.class);
	private PrintOutput output = new PrintOutput();

	// @Test
	// public void testRequestMultipleNames() {
	// int numberOfPlayers = 3;
	// String expected =
	// "Please enter the names of the 3 players. Press \"Enter\" after each name: ";
	// String actual = input.enterNamesOfMultiplePlayers(numberOfPlayers);
	// Assert.assertEquals(expected, actual);
	// }

	// @Test
	// public void testRequestOneName() {
	// int numberOfPlayers = 1;
	// Assert.assertEquals("Please enter the name of the 1 player: ",
	// input.enterNameOfOnePlayer(numberOfPlayers));
	// }

	@Test
	public void testMoreThanTwoPlayersOutput() {
		int numberOfPlayers = 3;
		String[] names = {"Nick", "Carly", "Chad"};
		Assert.assertEquals("The players are: Nick, Carly, and Chad.",
				output.printMoreThanTwoNames(names, numberOfPlayers));
	}

	@Test
	public void testTwoPlayersOutput() {
		int numberOfPlayers = 2;
		String[] names = {"Nick", "Carly"};
		Assert.assertEquals("The players are: Nick and Carly.",
				output.printTwoNames(names, numberOfPlayers));
	}

	@Test
	public void testSinglePlayerOutput() {
		int numberOfPlayers = 1;
		String[] names = {"Nick"};
		Assert.assertEquals("The player is: Nick.",
				output.printOneName(names, numberOfPlayers));
	}

	@Test
	public void testPerfectGame() {
		List<Integer> rolls = new ArrayList<Integer>();
		int numberOfPlayers = 1;
		int p = 0;
		int[] totalScore = new int[numberOfPlayers];
		for (int i = 0; i < 12; i++) {
			rolls.add(10);
		}
		int score = calculator.score(rolls, totalScore, p);
		Assert.assertEquals(300, score);
	}

	@Test
	public void testGutterGame() {
		List<Integer> rolls = new ArrayList<Integer>();
		int numberOfPlayers = 1;
		int p = 0;
		int[] totalScore = new int[numberOfPlayers];
		for (int i = 0; i < 20; i++) {
			rolls.add(0);
		}
		int score = calculator.score(rolls, totalScore, p);
		Assert.assertEquals(0, score);
	}

	@Test
	public void testFourGame() {
		List<Integer> rolls = new ArrayList<Integer>();
		int numberOfPlayers = 1;
		int p = 0;
		int[] totalScore = new int[numberOfPlayers];
		for (int i = 0; i < 20; i++) {
			rolls.add(4);
		}
		int score = calculator.score(rolls, totalScore, p);
		Assert.assertEquals(80, score);
	}

	@Test
	public void testFiveGame() {
		List<Integer> rolls = new ArrayList<Integer>();
		int numberOfPlayers = 1;
		int p = 0;
		int[] totalScore = new int[numberOfPlayers];
		for (int i = 0; i < 21; i++) {
			rolls.add(5);
		}
		int score = calculator.score(rolls, totalScore, p);
		Assert.assertEquals(150, score);
	}

	@Test
	public void testRandomGame() {
		List<Integer> rolls = new ArrayList<Integer>();
		int numberOfPlayers = 1;
		int p = 0;
		int[] totalScore = new int[numberOfPlayers];
		int[] rollArray = new int[]{0, 7, 10, 10, 3, 4, 9, 1, 4, 0, 10, 8, 2,
				1, 6, 10, 3, 5};
		for (int r : rollArray) {
			rolls.add(r);
		}
		int score = calculator.score(rolls, totalScore, p);
		Assert.assertEquals(128, score);
	}

}
