package com.summatech.bpalac.newbowling;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import com.summatech.bpalac.newbowling.calculation.BowlingCalculator;
import com.summatech.bpalac.newbowling.managers.ScoreManager;
import com.summatech.bpalac.newbowling.managers.TiesManager;
import com.summatech.bpalac.newbowling.messages.AskForInput;
import com.summatech.bpalac.newbowling.messages.PrintOutput;
import com.summatech.bpalac.newbowling.messages.ReadInput;

public class BowlingGameTest {

	private BowlingGame game;

	private BowlingCalculator calculatorMock;
	private AskForInput inputMock;
	private PrintOutput outputMock;
	private ReadInput readMock;
	private ScoreManager scoreManagerMock;
	private TiesManager tiesManagerMock;
	private Validations validationsMock;

	@Before
	public void setup() {
		calculatorMock = mock(BowlingCalculator.class);
		inputMock = mock(AskForInput.class);
		outputMock = mock(PrintOutput.class);
		readMock = mock(ReadInput.class);
		scoreManagerMock = mock(ScoreManager.class);
		tiesManagerMock = mock(TiesManager.class);
		validationsMock = mock(Validations.class);

		game = new BowlingGame(calculatorMock, inputMock, outputMock, readMock,
				scoreManagerMock, tiesManagerMock, validationsMock);
	}

	private void setupStartTest(int numberOfPlayers, String[] names) {
		when(validationsMock.getCorrectNamesValidation()).thenReturn(true);
		when(validationsMock.numberOfPlayersValidation()).thenReturn(
				numberOfPlayers);
		when(readMock.getNamesOfPlayers(numberOfPlayers)).thenReturn(names);
	}

	private void verifyPostStart(int numberOfPlayers, String[] names) {
		verify(inputMock).enterNumberOfPlayers();
		verify(validationsMock).numberOfPlayersValidation();
		verify(scoreManagerMock).initRollsList(numberOfPlayers);
		verify(readMock).getNamesOfPlayers(numberOfPlayers);
		verify(inputMock).askIfCorrect();
		verify(validationsMock).getCorrectNamesValidation();
	}

	@Test
	public void testStartWithMultiplePlayers() {
		int numberOfPlayers = 5;
		String[] names = {"Johnny", "Susan", "Pam", "Albert", "Wanda"};

		setupStartTest(numberOfPlayers, names);

		game.start(true);

		verifyPostStart(numberOfPlayers, names);
		verify(inputMock).enterNamesOfMultiplePlayers(numberOfPlayers);
		verify(outputMock).printMoreThanTwoNames(names, numberOfPlayers);
	}

	@Test
	public void testStartWithTwoPlayers() {
		int numberOfPlayers = 2;
		String[] names = {"Johnny", "Susan"};

		setupStartTest(numberOfPlayers, names);

		game.start(true);
		verifyPostStart(numberOfPlayers, names);
		verify(inputMock).enterNamesOfMultiplePlayers(numberOfPlayers);
		verify(outputMock).printTwoNames(names, numberOfPlayers);
	}

	@Test
	public void testStartWithSinglePlayer() {
		int numberOfPlayers = 1;
		String[] names = {"Johnny"};

		setupStartTest(numberOfPlayers, names);

		game.start(true);

		verifyPostStart(numberOfPlayers, names);
		verify(inputMock).enterNameOfOnePlayer(numberOfPlayers);
		verify(outputMock).printOneName(names, numberOfPlayers);
	}
}
