package com.summatech.bpalac.tddbowling;

public class BowlingGame implements Game {

	Input input;
	Messages messages;
	Validations validations;

	public BowlingGame(Input input, Messages messages, Validations validations) {
		this.input = input;
		this.messages = messages;
		this.validations = validations;
	}

	@Override
	public void start() {

		int numberOfPlayers = askForNumberOfPlayers();
		askForNamesOfPlayers(numberOfPlayers);

	}

	int askForNumberOfPlayers() {
		boolean isValid = false;
		int numberOfPlayers = 0;

		messages.numberOfPlayersPrompt();

		while (!isValid) {
			numberOfPlayers = input.getInteger();
			isValid = validations.validateNumberOfPlayers(numberOfPlayers);
			if (!isValid) {
				messages.invalidNumberOfPlayers();
			}
		}

		return numberOfPlayers;
	}

	String[] askForNamesOfPlayers(int numberOfPlayers) {
		boolean isCorrect = false;
		String yesOrNo;
		String[] namesOfPlayers = new String[numberOfPlayers];

		while (!isCorrect) {
			if (numberOfPlayers > 1) {
				messages.namesOfMultiplePlayersPrompt(numberOfPlayers);
			}
			else {
				messages.nameOfSinglePlayerPrompt(numberOfPlayers);
			}

			for (int currentPlayer = 0; currentPlayer < numberOfPlayers; currentPlayer++) {
				namesOfPlayers[currentPlayer] = input.getName();
			}

			if (numberOfPlayers > 2) {
				messages.printNamesOfMultiplePlayers(namesOfPlayers);
			}
			else if (numberOfPlayers == 2) {
				messages.printNamesOfTwoPlayers(namesOfPlayers);
			}
			else {
				messages.printNameOfSinglePlayer(namesOfPlayers);
			}

			if (numberOfPlayers > 1) {
				messages.askIfNamesAreCorrect();
			}
			else {
				messages.askIfNameIsCorrect();
			}

			yesOrNo = input.getCorrectNames();
			isCorrect = validations.validateNamesOfPlayers(yesOrNo);
		}

		return namesOfPlayers;
	}
}
