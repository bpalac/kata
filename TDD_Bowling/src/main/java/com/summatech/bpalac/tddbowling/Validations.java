package com.summatech.bpalac.tddbowling;

public class Validations {

	public static final int MIN_NUM_PLAYERS = 1;
	public static final int MAX_NUM_PLAYERS = 6;

	private Messages messages;

	public Validations(Messages messages) {
		this.messages = messages;
	}
	public boolean validateNumberOfPlayers(int numberOfPlayers) {
		if (numberOfPlayers < MIN_NUM_PLAYERS
				|| numberOfPlayers > MAX_NUM_PLAYERS) {
			return false;
		}
		return true;
	}

	public boolean validateNamesOfPlayers(String yesOrNo) {
		boolean isValid = false;
		boolean isCorrect = false;

		while (!isValid) {
			if (yesOrNo.equalsIgnoreCase("yes")
					|| yesOrNo.equalsIgnoreCase("y")) {
				isValid = true;
				isCorrect = true;
			}
			else if (yesOrNo.equalsIgnoreCase("no")
					|| yesOrNo.equalsIgnoreCase("n")) {
				isValid = true;
				isCorrect = false;
			}
			else {
				isValid = false;
				messages.invalidYesOrNo();
			}
		}
		return isCorrect;
	}

}
