package com.summatech.bpalac.tddbowling;

public class Application {

	public static Game game = GameFactory.buildBowlingGame();

	public static void main(String[] args) {
		game.start();
	}

}
