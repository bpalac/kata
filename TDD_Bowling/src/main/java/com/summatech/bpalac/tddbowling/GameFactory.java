package com.summatech.bpalac.tddbowling;

public class GameFactory {

	public static Game buildBowlingGame() {
		BowlingScanner sc = new BowlingScanner();
		Messages messages = new Messages();
		Input input = new Input(sc);
		Validations validations = new Validations(messages);
		return new BowlingGame(input, messages, validations);
	}

}
