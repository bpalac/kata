package com.summatech.bpalac.tddbowling;

public class Input {

	private BowlingScanner scanner;

	public Input(BowlingScanner scanner) {
		this.scanner = scanner;
	}

	public int getInteger() {
		return scanner.nextInt();
	}

	public String getName() {
		return scanner.next();
	}

	public String getCorrectNames() {
		return scanner.nextChar();
	}

}
