package com.summatech.bpalac.tddbowling;

import java.util.Scanner;

/**
 * Wrapper class for java.util.Scanner
 * 
 * @author bpalac
 *
 */

public class BowlingScanner {

	private Scanner scanner = new Scanner(System.in);

	public int nextInt() {
		if (!scanner.hasNextInt()) {
			return -1;
		}
		return scanner.nextInt();
	}

	public String next() {
		return scanner.next();
	}

	public String nextChar() {
		if (scanner.hasNextInt()) {
			return "Invalid entry";
		}
		else {
			return scanner.next();
		}
	}

}
