package com.summatech.bpalac.tddbowling;

public class Messages {

	public String numberOfPlayersPrompt() {
		return "Please enter the number of players: ";
	}

	public String invalidNumberOfPlayers() {
		return "Error! Please enter a number between "
				+ Validations.MIN_NUM_PLAYERS + " and "
				+ Validations.MAX_NUM_PLAYERS + ": ";
	}

	public String namesOfMultiplePlayersPrompt(int numberOfPlayers) {
		return "Please enter the names of the " + numberOfPlayers
				+ " players (Press \"Enter\" after each name.): ";
	}

	public String nameOfSinglePlayerPrompt(int numberOfPlayers) {
		return "Please enter the name of the " + numberOfPlayers + " player: ";
	}

	public String printNamesOfMultiplePlayers(String[] namesOfPlayers) {
		String printedNames = "The names of the players are: ";
		for (int currentPlayer = 0; currentPlayer < namesOfPlayers.length - 2; currentPlayer++) {
			printedNames = printedNames.concat(namesOfPlayers[currentPlayer]
					+ ", ");
		}
		printedNames = printedNames.concat(" and "
				+ namesOfPlayers[namesOfPlayers.length - 1] + ".");
		return printedNames;
	}
	public String printNamesOfTwoPlayers(String[] namesOfPlayers) {
		return "The names of the players are: " + namesOfPlayers[0] + " and "
				+ namesOfPlayers[1] + ".";
	}

	public String printNameOfSinglePlayer(String[] namesOfPlayers) {
		return "The name of the player is: " + namesOfPlayers[0] + ".";
	}

	public String askIfNamesAreCorrect() {
		return "Are these names correct? (Type \"Y\" for yes or \"N\" for no.): ";
	}

	public String askIfNameIsCorrect() {
		return "Is this name correct? (Type \"Y\" for yes or \"N\" for no.): ";
	}

	public String invalidYesOrNo() {
		return "Error! Please enter \"Y\" for yes or \"N\" for no: ";
	}
}
