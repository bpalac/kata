package com.summatech.bpalac.tddbowling;

import static com.summatech.bpalac.tddbowling.Validations.MAX_NUM_PLAYERS;
import static com.summatech.bpalac.tddbowling.Validations.MIN_NUM_PLAYERS;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.mockito.Mockito;

public class ValidationsTest {

	@Test
	public void testForInvalidIntegers() {
		Messages messagesMock = Mockito.mock(Messages.class);
		Validations validations = new Validations(messagesMock);

		assertFalse(validations.validateNumberOfPlayers(-1));
		assertFalse(validations.validateNumberOfPlayers(MIN_NUM_PLAYERS - 1));

		for (int i = MIN_NUM_PLAYERS; i <= MAX_NUM_PLAYERS; i++) {
			assertTrue(validations.validateNumberOfPlayers(i));
		}

		assertFalse(validations.validateNumberOfPlayers(MAX_NUM_PLAYERS + 1));
	}

	@Test
	public void testForInvalidYesOrNo() {
		Messages messagesMock = Mockito.mock(Messages.class);
		Validations validations = new Validations(messagesMock);

		assertTrue(validations.validateNamesOfPlayers("YES"));
		assertTrue(validations.validateNamesOfPlayers("Yes"));
		assertTrue(validations.validateNamesOfPlayers("yes"));
		assertTrue(validations.validateNamesOfPlayers("Y"));
		assertTrue(validations.validateNamesOfPlayers("y"));

		assertFalse(validations.validateNamesOfPlayers("NO"));
		assertFalse(validations.validateNamesOfPlayers("No"));
		assertFalse(validations.validateNamesOfPlayers("no"));
		assertFalse(validations.validateNamesOfPlayers("N"));
		assertFalse(validations.validateNamesOfPlayers("n"));
	}
}
