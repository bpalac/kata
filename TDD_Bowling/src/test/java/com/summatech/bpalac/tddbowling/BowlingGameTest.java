package com.summatech.bpalac.tddbowling;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class BowlingGameTest {

	private BowlingGame sut;
	private Input inputMock;
	private Messages messagesMock;
	private Validations validationsMock;

	private int numberOfPlayers;
	private String[] namesOfPlayers;

	@Before
	public void setup() {
		inputMock = mock(Input.class);
		messagesMock = mock(Messages.class);
		validationsMock = mock(Validations.class);
		sut = new BowlingGame(inputMock, messagesMock, validationsMock);

		numberOfPlayers = 5;
		namesOfPlayers = new String[]{"Steve", "Betty", "Siegfried", "Jekyll",
				"Quincy"};
		String yesOrNo = "y";

		when(inputMock.getInteger()).thenReturn(numberOfPlayers);
		when(validationsMock.validateNumberOfPlayers(numberOfPlayers))
				.thenReturn(true);
		when(inputMock.getName()).thenReturn("Steve").thenReturn("Betty")
				.thenReturn("Siegfried").thenReturn("Jekyll")
				.thenReturn("Quincy");
		when(validationsMock.validateNamesOfPlayers(yesOrNo)).thenReturn(true);
	}
	@Test
	public void startCallsAskForNumberOfPlayers() {
		sut = Mockito.spy(sut);

		sut.start();

		verify(sut).askForNumberOfPlayers();
	}

	@Test
	public void askForNumberOfPlayers() {
		sut.askForNumberOfPlayers();

		verify(messagesMock).numberOfPlayersPrompt();
		verify(validationsMock).validateNumberOfPlayers(numberOfPlayers);
	}

	@Test
	public void numberOfPlayersValid() {
		// if valid, should return numberOfPlayers
		int input = sut.askForNumberOfPlayers();

		Assert.assertEquals(numberOfPlayers, input);
	}

	@Test
	public void numberOfPlayersInvalid() {
		// if invalid, should ask for number of players again
		when(inputMock.getInteger()).thenReturn(numberOfPlayers);
		when(validationsMock.validateNumberOfPlayers(numberOfPlayers))
				.thenReturn(false).thenReturn(true);

		sut.askForNumberOfPlayers();

		verify(messagesMock, times(1)).numberOfPlayersPrompt();
		verify(validationsMock, times(2)).validateNumberOfPlayers(
				numberOfPlayers);
		verify(messagesMock, times(1)).invalidNumberOfPlayers();
	}

	@Test
	public void startCallsAskForNamesOfPlayers() {
		sut = Mockito.spy(sut);

		sut.start();

		verify(sut).askForNamesOfPlayers(numberOfPlayers);
	}

	@Test
	public void askForNameOfSinglePlayer() {
		numberOfPlayers = 1;
		namesOfPlayers = new String[]{"Siegfried"};

		when(inputMock.getName()).thenReturn("Siegfried");

		sut.askForNamesOfPlayers(numberOfPlayers);

		verify(messagesMock).nameOfSinglePlayerPrompt(numberOfPlayers);
		verify(inputMock, times(1)).getName();
		verify(messagesMock).printNameOfSinglePlayer(namesOfPlayers);
		verify(messagesMock).askIfNameIsCorrect();
	}

	@Test
	public void askForNamesOfTwoPlayers() {
		numberOfPlayers = 2;
		namesOfPlayers = new String[]{"Siegfried", "Jekyll"};

		when(inputMock.getName()).thenReturn("Siegfried").thenReturn("Jekyll");

		sut.askForNamesOfPlayers(numberOfPlayers);

		verify(messagesMock).namesOfMultiplePlayersPrompt(numberOfPlayers);
		verify(inputMock, times(2)).getName();
		verify(messagesMock).printNamesOfTwoPlayers(namesOfPlayers);
		verify(messagesMock).askIfNamesAreCorrect();
	}

	@Test
	public void askForNamesOfMultiplePlayers() {
		sut.askForNamesOfPlayers(numberOfPlayers);

		verify(messagesMock).namesOfMultiplePlayersPrompt(numberOfPlayers);
		verify(inputMock, times(5)).getName();
		verify(messagesMock).printNamesOfMultiplePlayers(namesOfPlayers);
		verify(messagesMock).askIfNamesAreCorrect();
	}

	@Test
	public void playerNamesValid() {
		String[] input = sut.askForNamesOfPlayers(numberOfPlayers);

		Assert.assertArrayEquals(namesOfPlayers, input);
	}

	@Test
	public void playerNamesInvalid() {
	}
}
