package com.summatech.bpalac.tddbowling;

import org.junit.Test;
import org.mockito.Mockito;

public class ApplicationTest {

	@Test
	public void testMainCallsGameStart() {
		Application.game = Mockito.mock(BowlingGame.class);
		Application.main(null);
		Mockito.verify(Application.game).start();
	}

}
