package com.summatech.bpalac.tddbowling;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class InputTest {

	@Test
	public void testGetInteger() {
		BowlingScanner scannerMock = Mockito.mock(BowlingScanner.class);
		Mockito.when(scannerMock.nextInt()).thenReturn(5);
		Input input = new Input(scannerMock);
		Assert.assertEquals(5, input.getInteger());
	}

	@Test
	public void testGetName() {
		BowlingScanner scannerMock = Mockito.mock(BowlingScanner.class);
		Mockito.when(scannerMock.next()).thenReturn("Steve");
		Input input = new Input(scannerMock);
		Assert.assertEquals("Steve", input.getName());
	}

	@Test
	public void testGetYesOrNo() {
		BowlingScanner scannerMock = Mockito.mock(BowlingScanner.class);
		Mockito.when(scannerMock.nextChar()).thenReturn("Yes");
		Input input = new Input(scannerMock);
		Assert.assertEquals("Yes", input.getCorrectNames());
	}
}
